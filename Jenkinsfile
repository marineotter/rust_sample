// パイプライン本体の宣言
pipeline {
  agent none
  options {
    skipDefaultCheckout() // Changelogが二重に記録されてしまう現象のワークアラウンド。最初の一発でstashして使い回すから、毎回checkoutする必要がない。
  }
  stages {
    stage('Checkout') {
      // ソースコードをチェックアウトする。（gitlabから指定した場合はgitlabからcloneします。）
      // チェックアウトしたソースコードは名前「src」でstash（保存）しておきます。
      // ここで保存したソースコードを次から使いまわしてビルドなどを実行します。
      agent {
        docker {
          image 'buildtools2019'
          label 'docker-windows'
        }
      }
      steps {
        ws("${env.WORKSPACE}/checkout") {
          deleteDir() // ワークスペースをきれいにしておく。
          checkout(changelog: true, scm: scm) // チェックアウトする。
          archiveArtifacts artifacts: 'Jenkinsfile', fingerprint: true // このJenkinsfileをいつでも見れるように保存しておく。
          stash name: 'src' // 名前「src」で保存する。
        }
      }
    }
    stage('Prepare env') {
      agent {
          label 'docker-windows'
      }
      steps {
        ws("${env.WORKSPACE}/dockerbuild") {
          deleteDir() // ワークスペースをきれいにしておく。
          unstash 'src' // 名前「src」で保存したやつを引っ張り出す。
          powershell 'cd ci_resources; cmd /c prepare.bat'
        }
      }
    }
    stage('Build') {
      parallel {
        stage('Build: Native Rust') {
          agent {
            docker {
              image 'buildtools2019rust'
              label 'docker-windows'
            }
          }
          steps {
            ws("${env.WORKSPACE}/rust") {
              deleteDir() // ワークスペースをきれいにしておく。
              unstash 'src' // 名前「src」で保存したやつを引っ張り出す。
              powershell 'cd native_rust; cmd /c build.bat'
              stash name: 'rust_target', includes: 'native_rust/target/**'
              recordIssues tool: cargo(id:'build_rust_x64', name: 'Build Rust x64', pattern: 'native_rust/build_x64.log', reportEncoding: 'utf8')
              recordIssues tool: cargo(id:'build_rust_x86', name: 'Build Rust x86', pattern: 'native_rust/build_x86.log', reportEncoding: 'utf8')
            }
          }
        }
        stage('Build: C#') {
          agent {
            docker {
              image 'buildtools2019'
              label 'docker-windows'
            }
          }
          steps {
            ws("${env.WORKSPACE}/csharp") {
              deleteDir()
              unstash 'src'
              powershell 'cd interface_csharp; cmd /c build.bat'
              stash name: 'csharp_binary', includes: 'interface_csharp/bin/**'
              recordIssues tool: msBuild(id:'build_cs_x64', name: 'Build C# x64', pattern: 'interface_csharp/build_x64.log'), sourceCodeEncoding: 'utf8'
              recordIssues tool: msBuild(id:'build_cs_x86', name: 'Build C# x86', pattern: 'interface_csharp/build_x86.log'), sourceCodeEncoding: 'utf8'
            }
          }
        }
      }
    }
    stage('Concat files') {
      agent {
        docker {
          image 'buildtools2019'
          label 'docker-windows'
        }
      }
      steps {
        ws("${env.WORKSPACE}/concat") {
          deleteDir()
          unstash 'rust_target'
          unstash 'csharp_binary'
          // 64bit
          powershell 'New-Item publish/x64 -ItemType Directory'
          powershell 'Copy-Item native_rust/target/x86_64-pc-windows-msvc/release/*.dll -Destination publish/x64/'
          powershell 'Copy-Item interface_csharp/bin/x64/Release/*.exe -Destination publish/x64/'
          powershell 'Copy-Item interface_csharp/bin/x64/Release/*.dll -Destination publish/x64/'
          powershell 'Copy-Item interface_csharp/bin/x64/Release/*.config -Destination publish/x64/'
          powershell 'Compress-Archive -Path publish/x64 -DestinationPath publish/x64.zip'
          // 32bit
          powershell 'New-Item publish/x86 -ItemType Directory'
          powershell 'Copy-Item native_rust/target/i686-pc-windows-msvc/release/*.dll -Destination publish/x86/'
          powershell 'Copy-Item interface_csharp/bin/x86/Release/*.exe -Destination publish/x86/'
          powershell 'Copy-Item interface_csharp/bin/x86/Release/*.dll -Destination publish/x86/'
          powershell 'Copy-Item interface_csharp/bin/x86/Release/*.config -Destination publish/x86/'
          powershell 'Compress-Archive -Path publish/x86 -DestinationPath publish/x86.zip'
          archiveArtifacts artifacts: 'publish/**', fingerprint: true
          archiveArtifacts artifacts: 'publish/*.zip', fingerprint: true
        }
      }
    }
  }
}
