echo off
cd %~dp0
REM buildtools2019が存在しなかった場合だけ、ビルドする。
FOR /F "DELIMS=" %%A IN ('docker images -q buildtools2019 ^| find /v "" /c') DO SET CNT=%%A
IF %CNT% GTR 0 (
    echo buildtools2019 exists. Nothing to do for this image.
) else (
    docker build -t buildtools2019:latest -m 4GB dockerfile.buildtools2019
)
REM buildtools2019rustが存在しなかった場合だけ、ビルドする。
FOR /F "DELIMS=" %%A IN ('docker images -q buildtools2019rust ^| find /v "" /c') DO SET CNT=%%A
IF %CNT% GTR 0 (
    echo buildtools2019rust exists. Nothing to do for this image.
) else (
    docker build -t buildtools2019rust:latest -m 4GB dockerfile.buildtools2019rust
)
