// ===================================================================
// get_primes の定義。 「///」スタートはドキュメントコメント。
/// ## 説明
/// 素数列挙の関数
/// ```
/// let count = lib::get_primes(limit, ret_buffer, ret_buffer_len);
/// ```
/// * limit ... 列挙する素数の最大値。例えば5を与えたら「2, 3, 5」を列挙する。
/// * ret_buffer ... 列挙した素数を格納するバッファへのポインタ。
/// * ret_buffer_len ... 列挙した素数を格納するバッファの上限サイズ。
///
/// バッファは列挙する総数に対して十分な値を持っている必要がある。持っていない場合、panicを生じる。
/// ## 備考
/// DLLにエクスポートしてP/Invokeなどから使用するため、インタフェースはC互換としている。
/// 32bit/64bit互換にするため、入出力はポインタを除きすべて32bit上限。
#[no_mangle]
pub extern "C" fn get_primes(
    limit: i32,           // 列挙する素数の上限
    ret_buffer: *mut i32, // バッファのポインタ
    ret_buffer_len: i32,  // バッファの上限（列挙数より大きい必要がある）
) -> i32 {
    let ret_buffer_slice: &mut [i32] = unsafe {
        assert!(!ret_buffer.is_null());
        std::slice::from_raw_parts_mut(ret_buffer, ret_buffer_len as usize)
    };
    let mut count: i32 = 0;
    if limit >= 2 {
        for val in 2..limit + 1 {
            let mut val_is_prime: bool = true;
            for j in 0..count {
                if val % ret_buffer_slice[j as usize] == 0 {
                    val_is_prime = false;
                    break;
                }
            }
            if val_is_prime {
                if ret_buffer_len <= count {
                    panic!();
                }
                ret_buffer_slice[count as usize] = val;
                count += 1;
            }
        }
    }
    return count;
}
