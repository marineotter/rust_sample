mod lib;

fn main() {
    // デバッグ用のランチャコード
    const CALC_LIMIT: i32 = 20;
    const BUF_LEN: i32 = 10;
    let buf = &mut [0 as i32; BUF_LEN as usize];
    let ret_count = lib::get_primes(CALC_LIMIT, buf.as_mut_ptr(), BUF_LEN);
    println!("{}", ret_count);
    for i in 0..ret_count {
        print!("{} ", buf[i as usize]);
    }
    println!();
}

#[test]
fn primes_is_ok_20() {
    // 20までの素数を列挙して、全てあってることを確認
    let correct = vec![2, 3, 5, 7, 11, 13, 17, 19];
    const CALC_LIMIT: i32 = 20;
    const BUF_LEN: i32 = 10;
    let buf = &mut [0 as i32; BUF_LEN as usize];
    let ret_count = lib::get_primes(CALC_LIMIT, buf.as_mut_ptr(), BUF_LEN);
    assert_eq!(buf[0..ret_count as usize], correct.as_slice()[..]);
}

#[test]
fn primes_time_ok_10000() {
    // 10000までの素数を列挙して計算時間が10秒以内に終わっていることを確認
    use std::time::Instant;

    let start = Instant::now();

    const CALC_LIMIT: i32 = 10000;
    const BUF_LEN: i32 = 2000;
    let mut buf = Vec::<i32>::with_capacity(BUF_LEN as usize);
    unsafe {
        buf.set_len(BUF_LEN as usize);
    }
    let res = lib::get_primes(CALC_LIMIT, buf.as_mut_ptr(), BUF_LEN);
    println!("{}", res);
    assert_eq!(buf[1], 3);

    let elapsed = start.elapsed();
    assert_eq!(elapsed.as_millis() < 10000, true);
}
