﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class RustFuncs
    {
        [DllImport("rustdll.dll", EntryPoint = "get_primes")]
        internal static extern Int32 GetPrimes(Int32 limit, IntPtr buf, Int32 bufLen);
    }
    class Program
    {
        static void Main(string[] args)
        {
            Int32 bufLen = 128;
            int[] result = new int[bufLen];
            IntPtr buffer = Marshal.AllocHGlobal((IntPtr)(sizeof(Int32) * bufLen));
            var res = RustFuncs.GetPrimes(20, buffer, bufLen);
            Console.WriteLine(String.Format("{0}", res));
            Marshal.Copy(buffer, result, 0, (int)res);
            for (int i = 0; i < (int)res; i++)
            {
                Console.Write(String.Format("{0} ", result[i]));
            }
            Console.WriteLine();
        }
    }
}
