call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat"

cd %~dp0
MSBuild ConsoleApp1.sln /t:clean;rebuild /p:Configuration=Release;Platform="X64" >build_x64.log
if %ERRORLEVEL% neq 0 (
    echo ErrorLevel:%ERRORLEVEL%
    exit /b 1
)
MSBuild ConsoleApp1.sln /t:clean;rebuild /p:Configuration=Release;Platform="X86" >build_x86.log
if %ERRORLEVEL% neq 0 (
    echo ErrorLevel:%ERRORLEVEL%
    exit /b 1
)
exit /b 0
